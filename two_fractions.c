//WAP to find the sum of two fractions.
#include<stdio.h>
struct fraction 
{
    int num;
    int deno;
};
typedef struct fraction Fract;
Fract input()
{
    Fract f;
    printf("Enter the numerator: ");
    scanf("%d",&f.num);
    printf("Enter the denominator: ");
    scanf("%d",&f.deno);
    return f;
}

Fract sum(Fract f1, Fract f2, Fract f3)
{
    int G;
    f3.num=(f1.num*f2.deno)+(f2.num*f1.deno);
    f3.deno=f1.deno*f2.deno;
    G =gcd(f3.num,f3.deno);
    f3.num= (f3.num)/G;
    f3.deno=(f3.deno)/G;
    return f3;
}

int gcd(int a, int b)
{
    int i,gcd=1,temp;
    if( a < b) 
    {
        temp = a;
        a=b;
        b=temp;
    }
    if ( a % b == 0) 
    {
        return b;
    }

    for ( i = b/2; i >=2 ; i--)
    {
        if( a%i == 0 && b%i==0)
	    return i;
    }
    return gcd;
}

void output(Fract f1, Fract f2, Fract f3)
{
    printf("\nThe sum of the two fractions %d/%d and %d/%d is %d/%d",f1.num,f1.deno,f2.num,f2.deno,f3.num,f3.deno);
}

int main()
{
    Fract f1, f2, f3;
    f1 = input();
    f2 = input();
    f3 = sum(f1,f2,f3);
    output(f1,f2,f3);
    return 0;
}