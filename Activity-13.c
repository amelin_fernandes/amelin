#include<stdio.h>
#include<math.h>

struct Point
{
  float x;
  float y;
};

typedef struct Point point;

struct rectangle
{
  point a[3];
  float area;
};

typedef struct rectangle rect;

int input()
{
    int n;
    printf("Enter the number of rectangles to be analyzed: ");
    scanf("%d",&n);
    return n;
}

void input_p(rect*one)
{
    for(int i=0;i<3;i++)
    {
        printf("Enter the value of point1:\n");
        scanf("%f",&one->a[i].x);
        printf("Enter the value of point2:\n");
        scanf("%f",&one->a[i].y);
    }
}

void input_n(rect a[],int n)
{
    for(int i=0; i<n; i++)
    input_p(&a[i]);
}

void compute(rect *one)
{
    float length=sqrt(pow((one->a[1].y-one->a[0].y),2)+pow((one->a[1].x-one->a[0].x),2));
    float breadth=sqrt(pow((one->a[0].y-one->a[2].y),2)+pow((one->a[0].x-one->a[2].x),2));
    one->area=length*breadth;
}

void compute_n(rect a[],int n)
{
    for(int i=0; i<n; i++)
        compute(&a[i]);
}
void output(rect *one)
{
    printf("Area of rectangle with vertices ");
    for(int i=0; i<3; i++)
        printf("(%.1f,%.1f) ",one->a[i].x,one->a[i].y);
    printf("is %.1f\n",one->area);
}

void output_n(rect a[],int n)
{
    for(int i=0; i<n; i++)
        output(&a[i]);
}

int main()
{
    int n;
    rect a[100];
    n = input();
    input_n(a,n);
    compute_n(a,n);
    output_n(a,n);
    return 0;
}