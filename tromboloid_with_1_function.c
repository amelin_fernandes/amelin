//Write a program to find the volume of a tromboloid using one function

#include<stdio.h>
int main()
{
    float h, d, b, x; 
    printf("Enter height: ");
    scanf("%f", &h);
    printf("Enter depth: ");
    scanf("%f", &d);
    printf("Enter breadth: ");
    scanf("%f", &b);
    x = ((h*d*b)+(d/b)) * 1/3;
    printf("Volume of tromboloid with height-%f, depth-%f and breadth-%f is: %f \n", h,d,b,x);
    return 0;
}