//Write a program to add two user input numbers using 4 functions.
#include<stdio.h>
int input()
{
    int a;
    printf("Enter number: \n");
    scanf("%d", &a);
    return a;
}

int sum(int a, int b)
{
    int sum;
    sum=a+b;
    return sum;
}

int output(int a,int b,int c)
{
    printf("Sum of %d and %d is %d \n", a,b,c);
}

int main()
{
    int t,u,v;
    t = input();
    u = input();
    v = sum(t,u);
    output(t,u,v);
    return 0;
}