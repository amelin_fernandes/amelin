//WAP to find the volume of a tromboloid using 4 functions.

#include<stdio.h>
float input()
{
    float a;
    printf("Enter the height, depth and breadth: ");
    scanf("%f", &a);
    return a;
}

float volume(float h, float d, float b)
{
    float v;
    v = ((h*d*b) + (d/b)) * 1/3 ;
    return v;
}

float output(float h, float d, float b, float c)
{
    printf("Volume of tromboloid with height-%f, depth-%f and breadth-%f is %f \n", h,d,b,c);
}

int main()
{
    float h,d,b,v;
    h= input();
    d= input();
    b= input();
    v= volume(h,d,b);
    output(h,d,b,v);
    return 0;
}
